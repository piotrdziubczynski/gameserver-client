<?php

declare(strict_types=1);

namespace App\Service;

final class Menu
{
    private $menu;

    public function __construct()
    {
        $this->menu = [
            'items' => [
                [
                    'item_classes' => '',
                    'item_name' => 'Główna',
                    'item_url' => '/home',
                ],
                [
                    'item_classes' => '',
                    'item_name' => 'Cennik',
                    'item_url' => '/price-list',
                ],
                [
                    'item_classes' => '',
                    'item_name' => 'Kontakt',
                    'item_url' => '/contact',
                ],
                [
                    'item_classes' => '',
                    'item_name' => 'FAQ',
                    'item_url' => '/faq',
                ],
                [
                    'item_classes' => '',
                    'item_name' => 'Wiki',
                    'item_url' => '/wikipedia',
                ],
            ],
            'special_items' => [
                [
                    'item_classes' => 'color red-gradient',
                    'item_icon' => 'log-in',
                    'item_name' => 'Logowanie',
                    'item_url' => '/sign-in',
                ],
                [
                    'item_classes' => 'color green-gradient',
                    'item_icon' => 'user-plus',
                    'item_name' => 'Rejestracja',
                    'item_url' => '/sign-up',
                ],
            ],
            'footer_items' => [
                [
                    'item_classes' => '',
                    'item_name' => 'Nasze banery',
                    'item_url' => '/banners',
                ],
                [
                    'item_classes' => '',
                    'item_name' => 'Współpraca',
                    'item_url' => '/cooperation',
                ],
                [
                    'item_classes' => '',
                    'item_name' => 'Regulamin',
                    'item_url' => '/regulations',
                ],
            ],
        ];
    }

    public function setActiveItem(int $activeItem): void
    {
        $activeItem -= 1;

        if (!isset($this->menu['items'][$activeItem])) {
            $activeItem = 0;
        }

        $this->menu['items'][$activeItem]['item_active'] = 'active';
    }

    public function get(string $type): array
    {
        return $this->menu[$type] ?? [];
    }

    public function __destruct()
    {
        $this->menu = null;
    }
}