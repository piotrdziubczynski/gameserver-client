<?php

declare(strict_types=1);

namespace App\Service;

use PDO;
use PDOException;

final class MyPDO extends PDO
{
    public function __construct()
    {
        $driver = getenv('DB_DRIVER') ?: 'mysql';
        $host = getenv('DB_HOST') ?: 'localhost';
        $dsn = sprintf('%s:host=%s;dbname=%s;charset=utf8', $driver, $host, getenv('DB_NAME'));

        try {
            parent::__construct($dsn, getenv('DB_USER'), getenv('DB_PASS'));
        } catch (PDOException $e) {
            die($e->getMessage());
        }

        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }
}
