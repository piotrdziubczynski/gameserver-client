<?php

declare(strict_types=1);

namespace App\Service;

final class Request
{
    private $params;

    public function __construct()
    {
        $this->params = [];

        foreach ($_REQUEST as $key => $value) {
            if (empty($key = $this->sanitizeValue($key))) {
                continue;
            }

            $this->params[$key] = $this->sanitizeValue($value);
        }
    }

    private function sanitizeValue($variable): string
    {
        return filter_var($variable, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_BACKTICK) ?: '';
    }

    public function getAll(): array
    {
        return $this->params;
    }

    public function get(string $paramKey, string $defaultValue = null): ?string
    {
        return $this->params[$paramKey] ?? $defaultValue;
    }

    public function getInt(string $paramKey, int $defaultValue = 0): int
    {
        return (int) $this->get($paramKey, (string) $defaultValue);
    }

    public function __destruct()
    {
        $this->params = null;
    }
}