<?php

declare(strict_types=1);

namespace App\Service\Manager;

use DateTime;

final class NewsManager extends AbstractManager
{
    public function getLatestNews(int $limit): array
    {
        \dd($this->repository->findUsers());

        // fake data; need to replace before production
        $news = [
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Hreczecha. Tu Kościuszko w pół godziny już w końcu dzieje chciano przeczyć.',
            ],
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Nic a co wzdłuż po francusku zwała karyjulka. Zamiast lokajów w naukach.',
            ],
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Bernardyna słyszałem, żeś się rumieniec oblekły. Tadeusz, na stosach Moskali siekąc wrogów.',
            ],
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Bernardyna słyszałem, żeś zza Niemna odebrał wiadomość. może nową alternat z urzędu.',
            ],
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Piramidy, w ulicę się echem i w okolicy. i na błoni i.',
            ],
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Na niem noty i na boku. Panny tuż nad uchem. Tadeusz przyglądał.',
            ],
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Wereszczaką, Giedrojć z pachnącymi ziołki geranium, lewkonija, astry i hec! od Moskali.',
            ],
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Podkomorzy i obyczaje, nawet o śmierci syna. Brał dom i musiał pochodzić.',
            ],
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Mościwego Pana zastępuje i światem. Dawniej na pacierz po rodzicach wziął najbliższą.',
            ],
            [
                'date' => (new DateTime())->format('d.m'),
                'text' => 'Dozoru tego nigdy sługom nie zbłądzi i konstytuować. Ogłosił nam, że się.',
            ],
        ];

        return array_slice($news, 0, $limit);
    }
}