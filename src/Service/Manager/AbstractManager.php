<?php

declare(strict_types=1);

namespace App\Service\Manager;

use App\Repository\RepositoryInterface;

abstract class AbstractManager
{
    /** @var RepositoryInterface */
    protected $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __destruct()
    {
        $this->repository = null;
    }
}