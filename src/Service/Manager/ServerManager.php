<?php

declare(strict_types=1);

namespace App\Service\Manager;

final class ServerManager extends AbstractManager
{
    public function getAllServers(): array
    {
        // fake data; need to replace before production
        $servers = [
            [
                'icon' => '',
                'name' => 'Counter Strike',
                'price' => 1.00,
                'settings' => [
                    'antyDdos' => true,
                    'antyDdosPrice' => 0,
                    'console' => true,
                    'autoinstall' => true,
                    'fastdl' => true,
                    'fastdlPrice' => 0,
                    'hltv' => true,
                ],
            ],
            [
                'icon' => '',
                'name' => 'Counter Strike Global Offensive',
                'price' => 2.40,
                'settings' => [
                    'antyDdos' => true,
                    'antyDdosPrice' => 0,
                    'console' => true,
                    'autoinstall' => true,
                    'fastdl' => true,
                    'fastdlPrice' => 0,
                    'hltv' => true,
                ],
            ],
            [
                'icon' => '',
                'name' => 'Rust',
                'price' => 1.30,
                'settings' => [
                    'antyDdos' => true,
                    'antyDdosPrice' => 0,
                    'console' => true,
                    'autoinstall' => true,
                    'fastdl' => true,
                    'fastdlPrice' => 0,
                    'hltv' => true,
                ],
            ],
            [
                'icon' => '',
                'name' => 'San Andreas Multiplayer',
                'price' => 0.30,
                'settings' => [
                    'antyDdos' => true,
                    'antyDdosPrice' => 0,
                    'console' => true,
                    'autoinstall' => true,
                    'fastdl' => true,
                    'fastdlPrice' => 0,
                    'hltv' => true,
                ],
            ],
        ];

        return $servers;
    }
}