<?php

declare(strict_types=1);

namespace App\Service;

use Mobile_Detect;

final class MobileDetect extends Mobile_Detect
{
    public function isSmartphone(): bool
    {
        return $this->isMobile() && !$this->isTablet();
    }
}