<?php

declare(strict_types=1);

namespace App\Repository;

use App\Service\MyPDO;

final class ServerRepository extends AbstractRepository
{
    public function __construct(MyPDO $pdo)
    {
        parent::__construct($pdo, 'servers');
    }

    public function findById(int $id)
    {
        // implementation here
    }
}