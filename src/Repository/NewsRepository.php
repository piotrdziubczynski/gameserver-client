<?php

declare(strict_types=1);

namespace App\Repository;

use App\Service\MyPDO;

final class NewsRepository extends AbstractRepository
{
    public function __construct(MyPDO $pdo)
    {
        parent::__construct($pdo, 'news');
    }

    public function findById(int $id)
    {
        // implementation here
    }

    public function findUsers()
    {
        return $this->pdo->query("SELECT `uuid`, `email`  FROM `users`")->fetchAll();
    }
}