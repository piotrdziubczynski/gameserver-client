<?php

declare(strict_types=1);

namespace App\Repository;

use App\Service\MyPDO;

abstract class AbstractRepository implements RepositoryInterface
{
    /** @var MyPDO */
    protected $pdo;
    /** @var string */
    protected $prefix;
    /** @var string */
    protected $tableName;

    public function __construct(MyPDO $pdo, string $tableName)
    {
        $this->pdo = $pdo;
        $this->prefix = getenv('DB_PREFIX') ?: '';
        $this->tableName = $tableName;
    }

    protected function getFullTableName(): string
    {
        return $this->prefix . $this->tableName;
    }

    public function __destruct()
    {
        $this->pdo = null;
        $this->prefix = null;
        $this->tableName = null;
    }
}