<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\MobileDetect;
use App\Service\Request;
use Throwable;
use Twig\Environment;

abstract class AbstractController
{
    /** @var Environment */
    private $twig;
    /** @var string */
    private $websiteName;
    /** @var MobileDetect */
    protected $device;
    /** @var array */
    protected $menus;
    /** @var bool */
    protected $isDevMode;
    /** @var Request */
    protected $request;

    public function __construct(Request $request, Environment $twig, MobileDetect $mobileDetect)
    {
        $this->menus = [];
        $this->websiteName = getenv('APP_NAME') ?: $_SERVER['HTTP_HOST'];
        $this->isDevMode = getenv('APP_ENV') !== 'prod';

        if ($this->isDevMode) {
            $twig->enableDebug();
        }

        $this->request = $request;
        $this->twig = $twig;
        $this->device = $mobileDetect;
    }

    protected function render(string $template, array $args = []): void
    {
        try {
            $this->twig->display($template, array_replace([
                'website_name' => $this->websiteName,
                'additional_class_list' => [],
            ], $this->menus, $args));
        } catch (Throwable $e) {
            die($e->getMessage());
        }
    }

    public function __destruct()
    {
        $this->device = null;
        $this->menus = null;
        $this->isDevMode = null;
        $this->request = null;
        $this->twig = null;
        $this->websiteName = null;
    }
}
