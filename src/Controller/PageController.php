<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\Manager\NewsManager;
use App\Service\Manager\ServerManager;
use App\Service\Menu;
use App\Service\MobileDetect;
use App\Service\Request;
use Twig\Environment;

final class PageController extends AbstractController
{
    /** @var Menu */
    private $menu;
    /** @var ServerManager */
    private $serverManager;
    /** @var NewsManager */
    private $newsManager;

    public function __construct(
        Request $request,
        Environment $twig,
        MobileDetect $mobileDetect,
        Menu $menu,
        ServerManager $serverManager,
        NewsManager $newsManager
    ) {
        parent::__construct($request, $twig, $mobileDetect);

        $this->menu = $menu;

        $this->menus = [
            'second_menu' => $this->menu->get('special_items'),
            'footer_menu' => $this->menu->get('footer_items'),
        ];

        $this->serverManager = $serverManager;

        $this->newsManager = $newsManager;
    }

    public function home() : void
    {
        $this->menu->setActiveItem(1);

        $newsLimit = 12;

        if ($this->device->isSmartphone()) {
            $newsLimit = 6;
        }

        $this->render('pages/index.html.twig', [
            'title' => 'Strona główna',
            'page_name' => 'Homepage',
            'menu' => $this->menu->get('items'),
            'servers' => $this->serverManager->getAllServers(),
            'news' => $this->newsManager->getLatestNews($newsLimit),
        ]);
    }

    public function __destruct()
    {
        parent::__destruct();

        $this->menu = null;

        $this->newsManager = null;

        $this->serverManager = null;
    }
}
