<?php

declare(strict_types=1);

use App\Controller\PageController;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

return simpleDispatcher(function (RouteCollector $route) {
    $route->addRoute(['GET'], '/[home]', [PageController::class, 'home']);
});
