<?php

/**
 * docs: http://php-di.org/doc/php-definitions.html
 */

declare(strict_types=1);

use App\Repository\NewsRepository;
use App\Repository\ServerRepository;
use App\Service\Manager\NewsManager;
use App\Service\Manager\ServerManager;
use App\Service\Menu;
use App\Service\MobileDetect;
use App\Service\MyPDO;
use App\Service\Request;
use Twig\Environment;
use Twig\Extension\CoreExtension;
use Twig\Loader\FilesystemLoader;

return [
    Environment::class => function () {
        $environment = new Environment(new FilesystemLoader(ROOT_DIR . '/templates'), [
            'strict_variables' => true,
        ]);

        $environment->getExtension(CoreExtension::class)->setNumberFormat(2, ',', ' ');

        return $environment;
    },

    Menu::class => DI\autowire(),
    MobileDetect::class => DI\autowire(),
    Request::class => DI\autowire(),

    MyPDO::class => DI\autowire(),
    ServerManager::class => DI\autowire()
        ->constructor(DI\get(ServerRepository::class)),
    NewsManager::class => DI\autowire()
        ->constructor(DI\get(NewsRepository::class)),
];
