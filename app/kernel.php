<?php

declare(strict_types=1);

require_once ROOT_DIR . '/vendor/autoload.php';

use DI\ContainerBuilder;
use Dotenv\Dotenv;

$dotenv = Dotenv::create(ROOT_DIR);

try {
    $dotenv->load();
    $dotenv->required('DB_NAME')->notEmpty();
    $dotenv->required('DB_USER')->notEmpty();
    $dotenv->required('DB_PASS');
} catch (\Throwable $e) {
    die($e->getMessage());
}

$builder = new ContainerBuilder();
$builder->addDefinitions(__DIR__ . '/autowire.php');

return $builder->build();
