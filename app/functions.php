<?php

declare(strict_types=1);

function dump(...$args)
{
  foreach ($args as $key => $arg) {
    echo '<pre>';
    var_dump($arg);
    echo '</pre>';
  }
}

function dd(...$args)
{
  dump(...$args);
  die();
}

function getUri(): string
{
  $uri = $_SERVER['REQUEST_URI'];
  $pos = strpos($uri, '?');

  if ($pos !== false) {
    $uri = substr($uri, 0, $pos);
  }

  return rawurldecode($uri);
}