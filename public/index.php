<?php

declare(strict_types=1);

define('ROOT_DIR', dirname(__DIR__));

use FastRoute\Dispatcher;

function invalidCode(int $code, array $allowMethods = []): void
{
    $message = '';

    switch ($code) {
        case Dispatcher::NOT_FOUND:
            $message = sprintf('%s Not Found', 404);
            header(sprintf('%s %s', $_SERVER["SERVER_PROTOCOL"], $message), true, 404);
            break;
        case Dispatcher::METHOD_NOT_ALLOWED:
            $message = sprintf('%s Method Not Allowed', 405);
            header(sprintf('Access-Control-Allow-Methods: %s', implode(', ', $allowMethods)), true, 405);
            break;
    }

    die($message);
}

require_once ROOT_DIR . '/app/functions.php';
$container = require_once ROOT_DIR . '/app/kernel.php';
$routes = require_once ROOT_DIR . '/app/routes.php';

$route = $routes->dispatch($_SERVER['REQUEST_METHOD'], getUri());

switch ($route[0]) {
    case Dispatcher::FOUND:
        $container->call($route[1], $route[2]);
        break;
    default:
        invalidCode($route[0], $route[1] ?? []);
        break;
}