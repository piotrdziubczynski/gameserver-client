(function ($) {
    'use strict';
    var windowWidth;

    $(document).ready(function () {
        windowWidth = $(window).width();

        if (windowWidth >= 1024) {
            $('.news-container').slick({
                arrows: false,
                cssEase: 'linear',
                dots: true,
                draggable: false,
                mobileFirst: true
            });
        }
    });
})(jQuery);