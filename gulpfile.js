/**
 * npm install
 */

"use strict";

const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    del = require('del'),
    babel = require('gulp-babel'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    bs = require('browser-sync').create();

function clean() {
    return del(['./public/app/']);
}

function browserSync(done) {
    bs.init({
        keepalive: true,
        open: true,
        proxy: 'http://evilhost.local:8080/'
    });
    done();
}

function browserSyncReload(done) {
    bs.reload();
    done();
}

function css() {
    return gulp.src('./assets/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(autoprefixer('> 0.5%', 'last 7 versions', 'Firefox ESR', 'not dead', 'not ie < 10'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public/app/css'));
}

function js() {
    return gulp.src('./assets/js/**/*.js')
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('./public/app/js'));
}

function images() {
    return gulp.src('./assets/images/**/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./public/app/images'));
}

function watchFiles() {
    gulp.watch('./assets/sass/**/*.scss', css);
    gulp.watch('./assets/js/**/*.js', js);
    gulp.watch('./assets/images/**/*', images);
    gulp.watch([
        './src/**/*.php',
        './templates/**/*.twig',
    ], gulp.series(browserSyncReload));
}

const build = gulp.series(clean, gulp.parallel(css, js, images));
const watch = gulp.parallel(watchFiles, browserSync);

exports.css = css;
exports.js = js;
exports.images = images;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = build;